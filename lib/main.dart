import 'package:flutter/material.dart';
import 'package:flutter_qr_scanner/page/promo_page.dart';
import 'package:flutter_qr_scanner/page/qr_scanner_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Demo App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  TabController _homeTabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _homeTabController = TabController(length: 3, initialIndex: 0, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: TabBarView(
        controller: _homeTabController,
        children: [
          new Container(
            child: PromoPage(),
          ),
          new Container(
            child: QRViewExample(),
          ),
          new Container(
            color: Colors.white,
          ),
        ],
      ),
      bottomNavigationBar: TabBar(
        controller: _homeTabController,
        tabs: [
          Tab(
            icon: new Icon(Icons.home),
            text: "Home",
          ),
          Tab(
            icon: new Icon(Icons.camera_alt),
            text: "Scan",
          ),
          Tab(
            icon: new Icon(Icons.perm_identity),
            text: "Profile",
          ),
        ],
        labelColor: Colors.lightBlueAccent,
        unselectedLabelColor: Colors.grey,
        indicatorSize: TabBarIndicatorSize.label,
        indicatorPadding: EdgeInsets.all(5.0),
        indicatorColor: Colors.lightBlueAccent,
      ),
    );
  }
}

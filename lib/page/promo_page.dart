import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PromoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PromoStatePage();
  }
}

class _PromoStatePage extends State<PromoPage> {
  final makeCard = Card(
    color: Colors.transparent,
    elevation: 8.0,
    margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
      decoration: new BoxDecoration(
          color: Color.fromRGBO(64, 75, 96, .9),
          borderRadius: new BorderRadius.all(Radius.circular(10.0))),
      child: ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child: Icon(Icons.autorenew, color: Colors.white),
          ),
          title: Text(
            "Promo",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

          subtitle: Row(
            children: <Widget>[
              Text(" deskripsi singkat promo",
                  style: TextStyle(color: Colors.white))
            ],
          ),
          trailing: Icon(Icons.keyboard_arrow_right,
              color: Colors.white, size: 30.0)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          makeCard,
          makeCard,
          makeCard,
          makeCard,
          makeCard,
          makeCard,
        ],
      ),
    );
  }
}
